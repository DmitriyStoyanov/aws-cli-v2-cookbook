# aws-cli-v2 CHANGELOG

This file is used to list changes made in each version of the aws-cli-v2 cookbook.

## 0.1.0

Initial release.

- added installation of aws-cli v2
- added tests for centos 6/7 and ubuntu 18 version
