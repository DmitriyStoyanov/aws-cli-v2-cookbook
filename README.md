# aws-cli-v2

Cookbook install aws-cli v2.

## Testing

- To test locally for all targets with Vagrant you can trigger

```shell
kitchen verify all
```

- To test locally for only specific target with Vagrant you can trigger

```shell
kitchen verify default-centos-6
```

- To test locally for all targets with docker you can trigger

```shell
KITCHEN_YAML=.kitchen.dokken.yml kitchen verify all
```