default['aws-cli-v2']['url'] = 'https://awscli.amazonaws.com'
default['aws-cli-v2']['version'] = '2.2.13'
default['aws-cli-v2']['prefix_dir'] = '/usr/local'
