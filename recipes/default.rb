#
# Cookbook:: aws-cli-v2
# Recipe:: default
#
# Copyright:: 2021, The Authors, All Rights Reserved.

version = node['aws-cli-v2']['version']
install_path = "#{node['aws-cli-v2']['prefix_dir']}/aws-cli/v2/#{version}"

package 'unzip'

remote_file "#{Chef::Config[:file_cache_path]}/awscli-exe-linux-x86_64-#{version}.zip" do
  source "#{node['aws-cli-v2']['url']}/awscli-exe-linux-x86_64-#{version}.zip"
  mode '0755'
  not_if { ::File.exist?(install_path) }
end

bash 'install-aws-cli-v2' do
  cwd Chef::Config[:file_cache_path]
  code <<-EOF
    unzip awscli-exe-linux-x86_64-#{version}.zip
    (cd aws && ./install)
  EOF
  not_if { ::File.exist?(install_path) }
end
