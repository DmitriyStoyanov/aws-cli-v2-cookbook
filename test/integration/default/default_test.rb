# InSpec test for recipe aws-cli-v2::default

# The InSpec reference, with examples and extensive documentation, can be
# found at https://www.inspec.io/docs/reference/resources/

describe file('/usr/local/bin/aws') do
  it { should exist }
  it { should be_symlink }
  it { should_not be_directory }
  it { should be_executable }
end

describe command('/usr/local/bin/aws --version') do
  its('exit_status') { should eq 0 }
  its('stderr') { should eq '' }
  its('stdout') { should include 'aws-cli' }
end
